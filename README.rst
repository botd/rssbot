RSSBOT is a IRC bot you can use to display RSS feeds.
RSSBOT is in the Public Domain and contains no copyright or LICENSE.

1) pip3 install rssbot
2) rssbot <server> <channel> <nick> 
3) !rss <url>
4) !fetch

